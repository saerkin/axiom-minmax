#pragma once

#undef min
#undef max

namespace axiom {

template<class T>
inline const T& min(const T& a, const T& b) {
  return a <= b ? a : b;
}

template<class T>
inline const T& max(const T& a, const T& b) {
  return a >= b ? a : b;
}

} // end of axiom namespace
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include "axiom/minmax.h"

using namespace axiom;

TEST_CASE("min test") {
  REQUIRE(min(0, 1) == 0);
  REQUIRE(min(0, 0) == 0);
  REQUIRE(min(-100, 1) == -100);

  REQUIRE(min(3.4, 4.0) == 3.4);
}

TEST_CASE("max test") {
  REQUIRE(max(0, 1) == 1);
  REQUIRE(max(0, 0) == 0);
  REQUIRE(max(-100, 1) == 1);

  REQUIRE(max(3.4, 4.0) == 4.0);
}